# pyanop

Visualize piano pieces as plot bars. Uses mido and matplotlib to convert midi files to graphs.
It's essentially a messier version of [Joey Cloud's pianogram](http://joeycloud.net/v/pianogram/).

## Installation

You will need `python3` and `pip3`

Clone the directory
```
git clone git@gitlab.com:t3Y/pyanop.git
```

Install dependencies
(you should probably use a virtual environment, but I have no clue how they work)

```
cd pyanop
pip3 install -r requirements.txt
```

## Usage

Use `python3 pyanop -h` for usage information:

usage: `pyanop.py [-h] [-v] [-x X] [-y Y] [-o OUTPUT] input_file`

