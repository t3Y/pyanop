#!/usr/bin/env python3

#  pyanop
#  Copyright (C) 2019 contact@t3Y.eu
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import argparse
import matplotlib.pyplot as plt
import matplotlib.image as image
from mido import MidiFile

verbose = False
path = os.path.dirname(__file__)

def main():
    total_notes = 0
    note_dist = [0]*88

    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="The midi file that should be used as input")
    parser.add_argument("-v", "--verbose", help="Display additional information while the program is running", action="store_true")
    parser.add_argument("-x", "--x", help="Specify the output image's width (inches)")
    parser.add_argument("-y", "--y", help="Specify the output image's height (inches)")
    parser.add_argument("-o", "--output", help="Specify the output image's name (without file extension)")
    parser.add_argument("-c", "--color", help="Specify the bars' color, provide the value as a html style hex string or html color name (e.g. #c1c1c1)")

    args = parser.parse_args()

    input_file = args.input_file

    # validate optional arguments

    if args.verbose:
        global verbose
        verbose = True

    output_width = 15
    output_height = 7

    try: 
        if args.x:
            output_width = int(args.x)
    except:
        print("Invalid value for argument -x")
    try:
        if args.y:
            output_height = int(args.y)
    except:
        print("Invalid value for argument -y")

    output_name = ""
    if args.output:
        output_name = "{}.png".format(args.output)

    try:
        mid = MidiFile(input_file)
    except:
        print("Error: Could not find file '{}'".format(input_file))
        sys.exit(1)

    color = "black"
    if args.color:
        color = args.color


    # count notes between 21 and 109

    for i, track in enumerate(mid.tracks):
        out("Track {}: {}".format(i, track.name))
        for msg in track:
            if msg.type == "note_on" and msg.velocity != 0:
                mid_note = msg.note
                if mid_note-21 > 0 and mid_note-21 < 88:
                    # note_dist contains notes offset by 21 (A0)
                    note_dist[mid_note-21] += 1
                out("{} ({})".format(mid_note, format_note(mid_note)))
                total_notes += 1

    out("Notes: {} ({}) unique".format(total_notes, len(note_dist)))
    out("Note distribution: {}".format(note_dist))

    if output_name == "":
        output_name = "{}.png".format(input_file.split(".")[0])

    gen_image(note_dist, output_width, output_height, output_name, color)


def gen_image(note_dist, output_width, output_height, output_name, color):

    # to ensure that the keyboard image always appears to have the same width
    # its y axis limit is a fraction of the longest bar's value
    
    img_scaling = max(note_dist) / 4
    im = image.imread(os.path.join(path, 'keyboard.png'))
    fig, ax = plt.subplots()
    ax.imshow(im, aspect='auto', extent=(0, 88, (-1 * img_scaling), 0))

    try: 
        plt.bar(range(len(note_dist)), note_dist, width=1, align='edge', facecolor=color, edgecolor=color)
    except ValueError:
        out("Invalid color provided, using default")
        plt.bar(range(len(note_dist)), note_dist, width=1, align='edge', facecolor="black", edgecolor="black")

    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)

    out("Generating {}x{} image".format(output_width, output_height))

    plt.box(False)
    plt.gcf().set_size_inches(output_width, output_height)
    plt.savefig(output_name, bbox_inches='tight', pad_inches=0, dpi=120)

    out("Done!")

# convert a midi note to its letter value
def format_note(note):
    notes = ("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B")
    output = notes[note % 12] + str(note // 12)
    return output

# print text only when --verbose is on
def out(arg):
    global verbose
    if verbose == True:
        print("[pyanop] {}".format(arg))

main()
